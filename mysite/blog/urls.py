from django.conf.urls import url , include
from blog.models import Post
from . import views
from django.views.generic import ListView , DetailView

urlpatterns = [
	url(r"^$",ListView.as_view(queryset=Post.objects.all().order_by("-date")[:25],template_name="blog/blog.html"),name="post_list"),
	url(r"^(?P<pk>\d+)$",DetailView.as_view(model=Post,template_name="blog/post.html")),
	url(r'^new/$', views.post_new, name='post_new'),
	url(r"^edit/(?P<pk>\d+)$", views.post_edit, name='post_edit'),
	url(r"^delete/(?P<pk>\d+)$", views.post_delete, name='post_delete'),
]
