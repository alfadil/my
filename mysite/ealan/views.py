from django.shortcuts import render,redirect,get_object_or_404
from .forms import PostForm,VisitorForm
from django.utils import timezone
from .models import Post,Category,Vender
from mysite import settings
import re

def post_details(request,pk):
	post = get_object_or_404(Post,pk = pk)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	if request.method == "GET":
		url = post.image.url
	return render(request, 'ealan/post.html', {'form': post,'urltoimage' : url,'categories':categories,'venders':venders})

def post_all(request):
	posts = Post.objects.all().order_by("-date")[:25]
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_cat(request,category):
	posts = Post.objects.filter(category=category)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_ven(request,vender):
	posts = Post.objects.filter(vender=vender)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_search(request):
	search_query = request.GET.get('search', None)
	posts = Post.objects.filter(title__contains = search_query)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

def post_ven_info(request,pk):
	vender = get_object_or_404(Vender,pk = pk)
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	#if request.method == "GET":
	#	url = post.image.url
	return render(request, 'ealan/venderinfo.html', {'form': vender,'categories':categories,'venders':venders})

def post_add_chart(request,pk):
	quantity = request.GET.get('quantity', None)
	post = get_object_or_404(Post,pk = pk)
	if quantity :
		price =  post.price * int(quantity)
		#request.session['price'] = price
		try:
			old_value = request.session['items']+":"
		except Exception:
			old_value = ""
			print(Exception)
		finally:
			request.session['items'] = old_value + pk+","+quantity+"," 

		try:
			old_price = request.session['price'] or 0
		except Exception:
			print("odl---------")
			old_price = 0
			print(Exception)
		finally:
			request.session['price'] = old_price + price

		print("asdddddddddddddddddddddddd",price)
	posts = Post.objects.all().order_by("-date")[:25]
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})


def request_chart(request):
	try:
		price = request.session['price']
		items = request.session['items']
	except Exception:
		price = ''
	if price != '':
		my_list = []
		for one in items.split(":"):
			ids = int(one.split(",")[0])
			quantity = int(one.split(",")[1])
			my_list.append(item(ids,quantity))


		print("---------------------",my_list[0].id)



	#posts = Post.objects.all().order_by("-date")[:25]
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/request.html', {'categories':categories,'venders':venders})

def getchart(request):
	try:
		price = request.session['price']
		items = request.session['items']
		print("ddddddddddddddddd",price)

		if price == None or items ==None:
			posts = Post.objects.all().order_by("-date")[:25]
			categories = Category.objects.all().order_by("name")
			venders = Vender.objects.all()
			return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})
		

	except Exception:
		posts = Post.objects.all().order_by("-date")[:25]
		categories = Category.objects.all().order_by("name")
		venders = Vender.objects.all()
		return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})
		
	if request.method == "POST":
		form = VisitorForm(request.POST)
		if form.is_valid():
			visitor = form.save(commit=False)
			visitor.date = timezone.now()
			visitor.total = price
			string = ""
			
			for one in items.split(":"):
			    ids = one.split(",")[0]
			    quantity = one.split(",")[1]
			    string += "product No :"+ids+"    the quantity"+quantity+"\n"

			visitor.orders = string
			print("---------------------------",visitor.orders)
			visitor.save()

			try:
				request.session['price'] = 0
				del request.session['items'] 
			except Exception:
				pass


			posts = Post.objects.all().order_by("-date")[:25]
			categories = Category.objects.all().order_by("name")
			venders = Vender.objects.all()
			return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})

		else:
			categories = Category.objects.all().order_by("name")
			return render(request, 'ealan/visitor.html', {'form': form,'categories':categories})
	else : 
		form = VisitorForm()
		categories = Category.objects.all().order_by("name")
		return render(request, 'ealan/visitor.html', {'form': form,'categories':categories})


def post_del_chart(request):
	try:
		request.session['price'] = 0
		del request.session['items'] 
	except Exception:
		pass
	
	posts = Post.objects.all().order_by("-date")[:25]
	categories = Category.objects.all().order_by("name")
	venders = Vender.objects.all()
	return render(request, 'ealan/posts.html', {'posts': posts,'categories':categories,'venders':venders})








class item :
	def __init__(self,id,quantity):
	 	self.id = id
	 	self.quantity = quantity 
