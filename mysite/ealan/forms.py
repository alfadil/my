from django.forms import ModelForm
from .models import Post,Vistor

class  PostForm(ModelForm):
	"""the form of the post table"""
	class Meta:
		model = Post
		fields  =('title','details','date','image','price','vender')

class  VisitorForm(ModelForm):
	class Meta:
		model = Vistor
		fields  =('name','Phone','email','Address')



			
		