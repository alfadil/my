from django.db import models
from django.contrib import admin
from django.core.validators import RegexValidator

class Post(models.Model):
	title   = models.CharField(max_length = 140)
	details = models.TextField()
	date    = models.DateTimeField()
	image   = models.ImageField()
	price   = models.FloatField()
	vender  = models.ForeignKey('Vender', on_delete=models.CASCADE)
	category= models.ForeignKey('Category', on_delete=models.CASCADE)
	def __str__(self):
		return "ID : "+str(self.id) +"     Name : "+ self.title 
					

class Vender(models.Model):
	name        = models.CharField(max_length = 140)
	Address     = models.CharField(max_length = 140)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	Phone1      = models.CharField(validators=[phone_regex], blank=True,max_length = 9) 
	Phone2      = models.CharField(validators=[phone_regex], blank=True,max_length = 9)
	email       = models.EmailField(max_length = 254)
	def __str__(self):
		return self.name
		
class Category(models.Model):
	name        = models.CharField(max_length = 140)
	def __str__(self):
		return self.name

class Vistor(models.Model):
	name        = models.CharField(max_length = 140)
	Address     = models.CharField(max_length = 140)
	date    = models.DateTimeField()
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	Phone     = models.CharField(validators=[phone_regex], blank=True,max_length = 9) 
	email       = models.EmailField(max_length = 254)
	orders      = models.TextField()
	total       = models.FloatField()
	def __str__(self):
		return "Name : "+str(self.name) +"/     Phone : "+ str(self.Phone) +"/     total : "+ str(self.total) 
