from django.conf.urls import url , include
from ealan.models import Post,Vender
from . import views
from django.views.generic import ListView , DetailView

urlpatterns = [
	#url(r"^$",ListView.as_view(queryset=Post.objects.all().order_by("-date")[:25],template_name="ealan/posts.html"),name="post_list"),
	url(r"^(?P<pk>\d+)$", views.post_details,  name='post_details'),
	url(r"^$", views.post_all,  name='post_all'),
	url(r"^category/(?P<category>\d+)$", views.post_cat,  name='post_cat'),
	url(r"^vender/(?P<vender>\d+)$", views.post_ven,  name='post_ven'),
	url(r"^search/?$", views.post_search,  name='post_search'),
	url(r"^venderinfo/(?P<pk>\d+)$", views.post_ven_info,  name='post_ven_info'),
	url(r"^addtochart/(?P<pk>\d+)?$", views.post_add_chart,  name='post_ven_info'),
	url(r"^getchart/$", views.getchart,  name='getchart'),
	url(r"^delchart/$", views.post_del_chart,  name='delchart'),
	    #r'^(?P<poll_id>\d+)/$'
	    #r"^(?P<category>\w+)", 
	
]
